db-up:
	cd infrastructure && docker-compose up

db-down:
	cd infrastructure && docker-compose down

web-dev:
	cd web-app && npm run dev

web-i:
	cd web-app && npm install

