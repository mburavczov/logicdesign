# Logic Design

Сервис создания и совместного редактирования диаграмм и блок-схем.
Реализована регистрация, создание, редактирование документа, управление доступом к документу

### Кейсы
- Сервис для совместной разработки блок-схем (кейс 10)

## Стек технологий

Бэкенд написан на [ASP.NET Core 6](https://dotnet.microsoft.com/apps/aspnet).
Фронтенд на [React](https://reactjs.org/) с использованием фреймворка [Next.js](https://nextjs.org/) 
и библиотек [Yjs](https://yjs.dev/), [MUI](https://mui.com/) и [React Flow](https://reactflow.dev/). 
Настроен CI/CD, API и web-сервис запускаются внутри [Docker](https://www.docker.com/) контейнера и 
разворачиваются на [Fabric.js](http://fabricjs.com/). В качестве БД используется [PostgreSQL](https://www.postgresql.org/).


## Демо

Видео работы: https://disk.yandex.ru/i/ZzeKOztAUy7NEg

API: https://logic-design-api.herokuapp.com/swagger/index.html  
WEB: https://logic-design.herokuapp.com/


## Развёртывание

Сервисы запускаются внутри докер контейнеров.
Команды нужно выпонять в корневой директории проекта.

Запуск API
```bash
docker build -f api/Dockerfile -t logic_design .
docker run -p 8080:8080 -e "PORT=8080" -it logic_design
```
Проверить запуск можно открыв swagger http://localhost:8080/swagger/index.html

Запуск web приложения
```bash
docker build -f web-app/Dockerfile -t ssnet_web .
docker run -p 8081:8081 -e "NEXTAUTH_URL='localhost:8081'" -e "PORT=8081" -it logic_design
```
Приложение будет доступно по адресу http://localhost:8081/