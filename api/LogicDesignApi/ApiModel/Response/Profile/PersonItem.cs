﻿namespace LogicDesignApi.ApiModel.Response.Profile
{
    public class PersonItem
    {
        public int Id { get; set; }
        public string Login { get; set; }
        public string Role { get; set; }
        public string State { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
    }
}