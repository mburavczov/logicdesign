﻿using LogicDesignApi.ApiModel.Response.Profile;

namespace LogicDesignApi.ApiModel.Response;

public class DocumentItem
{
    public string Id { get; set; }
    public string Name { get; set; }
    public int? OwnerId { get; set; }
    public string Settings { get; set; }
    public DateTime CreatedAt { get; set; }
    public DateTime? UpdatedAt { get; set; }
    public DateTime? DeletedAt { get; set; }
    public PersonItem Owner { get; set; }
    public List<PersonItem> Members { get; set; }
}