﻿namespace LogicDesignApi.ApiModel.Request.Document;

public class RenameDocumentRequest
{
    public string DocumentId { get; set; }
    public string Name { get; set; }
}