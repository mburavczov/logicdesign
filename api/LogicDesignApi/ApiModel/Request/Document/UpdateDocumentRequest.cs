﻿namespace LogicDesignApi.ApiModel.Request.Document;

public class UpdateDocumentRequest
{
    public string DocumentId { get; set; }
    public string Settings { get; set; }
}