﻿namespace LogicDesignApi.ApiModel.Request.Document;

public class CreateDocumentRequest
{
    public string? Name { get; set; }
}