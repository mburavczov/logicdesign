﻿namespace LogicDesignApi.ApiModel.Request.Document;

public class DeleteMemberRequest
{
    public string DocumentId { get; set; }
    public int PersonId { get; set; }
}