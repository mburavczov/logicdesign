﻿namespace LogicDesignApi.ApiModel.Request.Document;

public class DeleteDocumentRequest
{
    public string DocumentId { get; set; }
}