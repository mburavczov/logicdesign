﻿namespace LogicDesignApi.ApiModel.Request.Document;

public class AddMemberRequest
{
    public string DocumentId { get; set; }
    public int PersonId { get; set; }
}