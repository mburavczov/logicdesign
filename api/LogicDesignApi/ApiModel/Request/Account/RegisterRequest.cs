﻿namespace LogicDesignApi.ApiModel.Request.Account
{
    public class RegisterRequest
    {
        public string Login { get; set; }
        public string Pass { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string? DocumentId { get; set; }
    }
}