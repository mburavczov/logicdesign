﻿using LogicDesignApi.Entity;
using Microsoft.EntityFrameworkCore;

namespace LogicDesignApi.Repository
{
    public class DocumentRepository : BaseRepository<Document>
    {
        public DocumentRepository(MainContext context) : base(context)
        {
        }

        public Document Get(string id)
        {
            return DbSet.Include(x => x.Owner)
                .Include(x => x.Persons)
                .FirstOrDefault(x => x.Id == id);
        }

        public List<Document> GetByUser(int personId)
        {
            return DbSet.Include(x => x.Owner)
                .Include(x => x.Persons)
                .Where(x => !x.DeletedAt.HasValue)
                .Where(x => x.OwnerId == personId || x.Persons.Any(p => p.Id == personId))
                .ToList();
        }

        public void Update(Document document)
        {
            DbSet.Update(document);
            Context.SaveChanges();
        }
    }
}