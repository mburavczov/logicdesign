﻿namespace LogicDesignApi.Entity
{
    public class Person
    {
        public Person()
        {
            Documents = new List<Document>();
            AvailableDocuments = new List<Document>();
        }
        
        public int Id { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public PersonRole Role { get; set; }
        public PersonState State { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        
        public virtual ICollection<Document> Documents { get; set; }
        public virtual ICollection<Document> AvailableDocuments { get; set; }
    }
}