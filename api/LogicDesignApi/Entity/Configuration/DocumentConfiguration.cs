﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace LogicDesignApi.Entity.Configuration
{
    public class DocumentConfiguration : IEntityTypeConfiguration<Document>
    {
        public void Configure(EntityTypeBuilder<Document> builder)
        {
            builder.HasKey(x => x.Id);

            builder.Property(x => x.Name).IsRequired();
            builder.Property(x => x.Name).HasMaxLength(100);
            
            builder.Property(x => x.CreatedAt).IsRequired();
            builder.Property(x => x.OwnerId).IsRequired();

            builder.HasOne(x => x.Owner)
                .WithMany(x => x.Documents)
                .HasForeignKey(x => x.OwnerId);

            builder.HasMany(x => x.Persons)
                .WithMany(x => x.AvailableDocuments)
                .UsingEntity(x => x.ToTable("PersonDocument"));
        }
    }
}