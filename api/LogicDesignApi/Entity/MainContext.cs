﻿using LogicDesignApi.Entity.Configuration;
using Microsoft.EntityFrameworkCore;

namespace LogicDesignApi.Entity
{
    public class MainContext : DbContext
    {
        public MainContext()
        {
            Database.EnsureCreated();
        }
 
        public MainContext(DbContextOptions options) : base(options) {}

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfiguration(new PersonConfiguration());
        }

        public DbSet<Person> Persons { get; set; }
        public DbSet<Document> Documents { get; set; }
    }
}