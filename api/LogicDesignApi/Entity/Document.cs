﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LogicDesignApi.Entity;

public class Document
{
    public Document()
    {
        Persons = new List<Person>();
    }
    
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    [Key]
    public string Id { get; set; }
    public string Name { get; set; }
    public int? OwnerId { get; set; }
    public string? Settings { get; set; }
    public DateTime CreatedAt { get; set; }
    public DateTime? UpdatedAt { get; set; }
    public DateTime? DeletedAt { get; set; }
    
    public virtual Person Owner { get; set; }
    public virtual ICollection<Person> Persons { get; set; }
}