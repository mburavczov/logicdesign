﻿using LogicDesignApi.ApiModel.Request.Document;
using LogicDesignApi.ApiModel.Response;
using LogicDesignApi.ApiModel.Response.Profile;
using LogicDesignApi.Entity;
using LogicDesignApi.Repository;
using LogicDesignApi.Settings;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace LogicDesignApi.Controllers
{
    /// <summary>
    /// Methods for create, update and retrieve documents
    /// </summary>
    [ApiController]
    [Route("api/[controller]")]
    public class DocumentController : Controller
    {
        private readonly DocumentRepository _documentRepository;
        private readonly PersonRepository _personRepository;

        public DocumentController(DocumentRepository documentRepository, PersonRepository personRepository)
        {
            _documentRepository = documentRepository;
            _personRepository = personRepository;
        }
        
        /// <summary>
        /// Create new document
        /// </summary>
        /// <returns>Document</returns>
        /// <response code="200">Ok</response>
        [HttpPost("Create")]
        [ProducesResponseType(typeof(DocumentItem), 200)]
        public IActionResult Create(CreateDocumentRequest model)
        {
            int? personId = null;
            if (int.TryParse(User?.FindFirst(x => x.Type == Constants.ClaimTypeUserId)?.Value, out var v))
            {
                personId = v;
            }
            
            var doc = new Document
            {
                Name = model.Name,
                OwnerId = personId,
                CreatedAt = DateTime.Now.ToUniversalTime()
            };
            if (string.IsNullOrEmpty(doc.Name))
            {
                doc.Name = "Новый документ";
            }

            _documentRepository.Add(doc);
            doc = _documentRepository.Get(doc.Id);
            
            return Ok(ToDocumentItem(doc));
        }
        
        /// <summary>
        /// Get document
        /// </summary>
        /// <returns>Document</returns>
        /// <response code="200">Ok</response>
        [HttpGet("Get")]
        [ProducesResponseType(typeof(DocumentItem), 200)]
        public IActionResult Get(string documentId)
        {
            int? personId = null;
            if (int.TryParse(User?.FindFirst(x => x.Type == Constants.ClaimTypeUserId)?.Value, out var v))
            {
                personId = v;
            }
            var doc = _documentRepository.Get(documentId);
            if (doc == null)
            {
                return NotFound();
            }

            if (doc.OwnerId.HasValue && doc.OwnerId != personId && doc.Persons.All(p => p.Id != personId))
            {
                return NotFound();
            }
            
            
            return Ok(ToDocumentItem(doc));
        }
        
        /// <summary>
        /// Get all my documents
        /// </summary>
        /// <returns>Documents</returns>
        /// <response code="200">Ok</response>
        [Authorize]
        [HttpGet("GetMy")]
        [ProducesResponseType(typeof(List<DocumentItem>), 200)]
        public IActionResult GetMy()
        {
            var personId = int.Parse(User?.FindFirst(x => x.Type == Constants.ClaimTypeUserId)?.Value);
            var docs = _documentRepository.GetByUser(personId);
            
            return Ok(docs.Select(ToDocumentItem).ToList());
        }
        
        /// <summary>
        /// Rename document
        /// </summary>
        /// <returns>Document</returns>
        /// <response code="200">Ok</response>
        /// <response code="40o">Document not available</response>
        /// <response code="404">Document not found</response>
        [Authorize]
        [HttpPost("Rename")]
        [ProducesResponseType(typeof(DocumentItem), 200)]
        public IActionResult Rename(RenameDocumentRequest model)
        {
            var doc = _documentRepository.Get(model.DocumentId);
            if (doc == null)
            {
                return NotFound();
            }

            if (doc.DeletedAt.HasValue)
            {
                return BadRequest();
            }

            doc.Name = model.Name;
            doc.UpdatedAt = DateTime.Now.ToUniversalTime();
            _documentRepository.Update(doc);
            
            return Ok(ToDocumentItem(doc));
        }
        
        /// <summary>
        /// Update document
        /// </summary>
        /// <returns>Document</returns>
        /// <response code="200">Ok</response>
        /// <response code="400">Document not available</response>
        /// <response code="404">Document not found</response>
        [Authorize]
        [HttpPost("Update")]
        [ProducesResponseType(typeof(DocumentItem), 200)]
        public IActionResult Update(UpdateDocumentRequest model)
        {
            var doc = _documentRepository.Get(model.DocumentId);
            if (doc == null)
            {
                return NotFound();
            }

            if (doc.DeletedAt.HasValue)
            {
                return BadRequest();
            }

            doc.Settings = model.Settings;
            doc.UpdatedAt = DateTime.Now.ToUniversalTime();
            _documentRepository.Update(doc);
            
            return Ok(ToDocumentItem(doc));
        }
        
        /// <summary>
        /// Delete document
        /// </summary>
        /// <response code="200">Ok</response>
        /// <response code="404">Document not found</response>
        [Authorize]
        [HttpPost("Delete")]
        public IActionResult Delete(DeleteDocumentRequest model)
        {
            var personId = int.Parse(User?.FindFirst(x => x.Type == Constants.ClaimTypeUserId)?.Value);
            var doc = _documentRepository.Get(model.DocumentId);
            if (doc == null)
            {
                return NotFound();
            }

            if (doc.OwnerId != personId)
            {
                return NotFound();
            }

            doc.DeletedAt = DateTime.Now.ToUniversalTime();
            _documentRepository.Update(doc);
            
            return Ok();
        }

        /// <summary>
        /// Add member to document
        /// </summary>
        /// <response code="200">Ok</response>
        /// <response code="400">Person not found</response>
        /// <response code="404">Document not found</response>
        [Authorize]
        [HttpPost("AddMember")]
        public ActionResult AddMember(AddMemberRequest model)
        {
            var doc = _documentRepository.Get(model.DocumentId);
            if (doc == null)
            {
                return NotFound();
            }

            var person = _personRepository.GetById(model.PersonId);
            if (person == null)
            {
                return BadRequest("person not fount");
            }
            
            doc.Persons.Add(person);
            _documentRepository.Update(doc);

            return Ok();
        }

        /// <summary>
        /// Delete member from document
        /// </summary>
        /// <response code="200">Ok</response>
        /// <response code="400">Person not found</response>
        /// <response code="404">Document not found</response>
        [Authorize]
        [HttpPost("DeleteMember")]
        public ActionResult DeleteMember(DeleteMemberRequest model)
        {
            var personId = int.Parse(User?.FindFirst(x => x.Type == Constants.ClaimTypeUserId)?.Value);
            var doc = _documentRepository.Get(model.DocumentId);
            if (doc == null)
            {
                return NotFound();
            }

            if (doc.OwnerId != personId)
            {
                return NotFound();
            }

            var person = doc.Persons.FirstOrDefault(x => x.Id == model.PersonId);
            if (person == null)
            {
                return BadRequest("person not found");
            }

            doc.Persons.Remove(person);
            _documentRepository.Update(doc);

            return Ok();
        }

        private DocumentItem ToDocumentItem(Document doc)
        {
            return new DocumentItem
            {
                Id = doc.Id,
                Name = doc.Name,
                OwnerId = doc.OwnerId,
                Settings = doc.Settings,
                CreatedAt = doc.CreatedAt,
                UpdatedAt = doc.UpdatedAt,
                DeletedAt = doc.DeletedAt,
                Owner = MapPerson(doc.Owner),
                Members = doc.Persons?.Select(MapPerson).ToList()
            };
        }

        private PersonItem MapPerson(Person person)
        {
            if (person == null)
            {
                return null;
            }
            var info = new PersonItem
            {
                Id = person.Id,
                Login = person.Login,
                Role = person.Role.Name(),
                State = person.State.Name(),
                Name = person.Name,
                Surname = person.Surname,
            };

            return info;
        }
    }
}
