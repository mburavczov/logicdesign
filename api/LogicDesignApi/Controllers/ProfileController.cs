﻿using LogicDesignApi.ApiModel.Response.Profile;
using LogicDesignApi.Entity;
using LogicDesignApi.Repository;
using LogicDesignApi.Settings;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace LogicDesignApi.Controllers
{
    /// <summary>
    /// Methods for manage profile
    /// </summary>
    [ApiController]
    [Route("api/[controller]")]
    public class ProfileController : Controller
    {
        private readonly PersonRepository _personRepository;

        public ProfileController(PersonRepository personRepository)
        {
            _personRepository = personRepository;
        }


        /// <summary>
        /// Get profile info
        /// </summary>
        /// <param name="personId">If not specified, the current id is used</param>
        /// <response code="404">Person not found</response>
        [Authorize]
        [HttpGet("Info")]
        [ProducesResponseType(typeof(PersonItem), 200)]
        public async Task<IActionResult> Info(int? personId)
        {
            if (!personId.HasValue)
            {
                personId = int.Parse(User?.FindFirst(x => x.Type == Constants.ClaimTypeUserId)?.Value);
            }

            var person = _personRepository.GetById(personId.Value);
            if (person == null)
            {
                return NotFound();
            }

            return Ok(MapPerson(person));
        }
        
        /// <summary>
        /// Get current profile info
        /// </summary>
        /// <response code="404">Person not found</response>
        [Authorize]
        [HttpGet("Me")]
        [ProducesResponseType(typeof(PersonItem), 200)]
        public async Task<IActionResult> Me()
        {
            var personId = int.Parse(User?.FindFirst(x => x.Type == Constants.ClaimTypeUserId)?.Value);
            var person = _personRepository.GetById(personId);
            if (person == null)
            {
                return NotFound();
            }

            return Ok(MapPerson(person));
        }

        /// <summary>
        /// Get all persons
        /// </summary>
        [Authorize]
        [HttpGet("Persons")]
        [ProducesResponseType(typeof(List<PersonItem>), 200)]
        public async Task<IActionResult> Persons()
        {
            var persons = _personRepository.GetAll();
            var res = new List<PersonItem>();
            foreach (var person in persons)
            {
                res.Add(MapPerson(person));
            }

            return Ok(res);
        }

        private PersonItem MapPerson(Person person)
        {
            var info = new PersonItem
            {
                Id = person.Id,
                Login = person.Login,
                Role = person.Role.Name(),
                State = person.State.Name(),
                Name = person.Name,
                Surname = person.Surname,
            };

            return info;
        }
    }
}