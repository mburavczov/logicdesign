﻿using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using LogicDesignApi.ApiModel.Request.Account;
using LogicDesignApi.ApiModel.Response.Account;
using LogicDesignApi.Entity;
using LogicDesignApi.Repository;
using LogicDesignApi.Settings;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;

namespace LogicDesignApi.Controllers
{
    /// <summary>
    /// Methods for register, authentication and authorization
    /// </summary>
    [ApiController]
    [Route("api/[controller]")]
    public class AccountController : Controller
    {
        private readonly PersonRepository _personRepository;
        private readonly DocumentRepository _documentRepository;

        public AccountController(PersonRepository personRepository, DocumentRepository documentRepository)
        {
            _personRepository = personRepository;
            _documentRepository = documentRepository;
        }

        /// <summary>
        /// Get JWT token by login and password
        /// </summary>
        /// <returns>JWT token</returns>
        /// <response code="200">Ok</response>
        /// <response code="400">Invalid username or password</response>
        [HttpPost("Token")]
        [ProducesResponseType(typeof(TokenResponse), 200)]
        public IActionResult Token([FromBody] TokenRequest data)
        {
            var person = _personRepository.Get(data.Login);
            if (person == null || !BCrypt.Net.BCrypt.Verify(data.Pass, person.Password))
            {
                return BadRequest(new { errorText = "Invalid username or password." });
            }

            if (!string.IsNullOrEmpty(data.DocumentId))
            {
                var doc = _documentRepository.Get(data.DocumentId);
                if (!doc.OwnerId.HasValue)
                {
                    doc.OwnerId = person.Id;
                    _documentRepository.Update(doc);
                }
            }

            return Json(new TokenResponse
            {
                Token = GetToken(person)
            });
        }

        /// <summary>
        /// Register new account
        /// </summary>
        /// <returns>Token</returns>
        /// <response code="200">Ok</response>
        [HttpPost("Register")]
        [ProducesResponseType(typeof(RegisterResponse), 200)]
        public IActionResult Register([FromBody] RegisterRequest data)
        {
            var p = _personRepository.Get(data.Login);
            if (p != null)
            {
                return BadRequest("login already exists");
            }
            
            var person = new Person
            {
                Login = data.Login,
                Password = BCrypt.Net.BCrypt.HashPassword(data.Pass),
                Name = data.Name,
                Surname = data.Surname,
                Role = PersonRole.User,
                State = PersonState.Active,
            };
            _personRepository.Add(person);

            if (!string.IsNullOrEmpty(data.DocumentId))
            {
                var doc = _documentRepository.Get(data.DocumentId);
                if (!doc.OwnerId.HasValue)
                {
                    doc.OwnerId = person.Id;
                    _documentRepository.Update(doc);
                }
            }

            return Ok(new RegisterResponse
            {
                Token = GetToken(person)
            });
        }

        private string GetToken(Person person)
        {
            var claims = new List<Claim>
            {
                new Claim(Constants.ClaimTypeName, person.Name),
                new Claim(Constants.ClaimTypeRole, person.Role.Name()),
                new Claim(Constants.ClaimTypeLogin, person.Login),
                new Claim(Constants.ClaimTypeUserId, person.Id.ToString())
            };
            claims.AddRange(GetIdentity(person).Claims);
            var now = DateTime.UtcNow;
            var jwt = new JwtSecurityToken(
                issuer: AuthOptions.ISSUER,
                audience: AuthOptions.AUDIENCE,
                notBefore: now,
                claims: claims,
                expires: now.Add(TimeSpan.FromMinutes(AuthOptions.LIFETIME)),
                signingCredentials: new SigningCredentials(AuthOptions.GetSymmetricSecurityKey(),
                    SecurityAlgorithms.HmacSha256));

            return new JwtSecurityTokenHandler().WriteToken(jwt);
        }
        
        private ClaimsIdentity GetIdentity(Person person)
        {
            if (person != null)
            {
                var claims = new List<Claim>
                {
                    new Claim(ClaimsIdentity.DefaultNameClaimType, person.Login),
                    new Claim(ClaimsIdentity.DefaultRoleClaimType, person.Role.Name())
                };
                ClaimsIdentity claimsIdentity =
                    new ClaimsIdentity(claims, "Token", ClaimsIdentity.DefaultNameClaimType,
                        ClaimsIdentity.DefaultRoleClaimType);
                return claimsIdentity;
            }

            return null;
        }
    }
}