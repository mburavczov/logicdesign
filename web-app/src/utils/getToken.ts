export function getToken() {
  if (typeof window === 'undefined') return undefined;

  const accessToken = window.localStorage.getItem('accessToken');

  return accessToken ? `Bearer ${accessToken}` : undefined;
}
