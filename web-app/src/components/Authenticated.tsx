import type { ReactNode } from 'react';
import { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import { useAuth } from '@/hooks';

interface AuthenticatedProps {
  children: ReactNode;
}

export function Authenticated({ children }: AuthenticatedProps) {
  const auth = useAuth();
  const router = useRouter();
  const [verified, setVerified] = useState(false);

  useEffect(() => {
    if (!router.isReady) {
      return;
    }

    if (!auth.isAuthenticated) {
      router.push({
        pathname: '/sign-in',
        query: { backTo: router.asPath }
      });
    } else {
      setVerified(true);
    }
  }, [router.isReady, auth.isAuthenticated]);

  if (!verified) {
    return null;
  }

  return <>{children}</>;
}
