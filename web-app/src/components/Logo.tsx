import Image from 'next/image';

interface LogoProps {
  size?: number;
}

export function Logo({ size = 40 }: LogoProps) {
  return (
    <Image
      objectPosition="center"
      alt="Logic Design"
      height={size}
      width={size}
      src="/static/logo.svg"
    />
  );
}
