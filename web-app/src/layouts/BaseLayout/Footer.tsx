import { Box, Card, Typography, styled } from '@mui/material';

export function Footer() {
  return (
    <FooterWrapper className="footer-wrapper">
      <Box
        p={4}
        display={{ xs: 'block', md: 'flex' }}
        alignItems="center"
        textAlign={{ xs: 'center', md: 'left' }}
        justifyContent="space-between"
      >
        <Box>
          <Typography variant="subtitle1">
            &copy; 2022 - Logic Design
          </Typography>
        </Box>
        <Typography
          sx={{
            pt: { xs: 2, md: 0 }
          }}
          variant="subtitle1"
        >
          Команда "Феникс"
        </Typography>
      </Box>
    </FooterWrapper>
  );
}

const FooterWrapper = styled(Card)(
  ({ theme }) => `
    border-radius: 0;
    margin-top: ${theme.spacing(8)};
    position: fixed;
    left: 0;
    bottom: 0;
    width: 100%;
    z-index: 6;
    box-shadow: ${theme.header.boxShadow};
`
);
