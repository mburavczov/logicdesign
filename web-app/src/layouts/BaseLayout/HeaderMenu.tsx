import { Box, List, ListItem, ListItemText, styled } from '@mui/material';
import { Link } from '@/components';

export function HeaderMenu() {
  const points = [
    {
      name: 'Мои схемы',
      href: '/dashboard'
    },
    { name: 'О проекте', href: '/' }
  ];

  return (
    <ListWrapper>
      <List disablePadding component={Box} display="flex">
        {points.map(({ href, name }) => (
          <CustomListItem
            key={href}
            component={Link}
            href={href}
            classes={{ root: 'MuiListItem-indicators' }}
          >
            <ListItemText
              primaryTypographyProps={{ noWrap: true }}
              primary={name}
            />
          </CustomListItem>
        ))}
      </List>
    </ListWrapper>
  );
}

const CustomListItem = styled(ListItem)`
  cursor: pointer;
  text-decoration: none !important;
  color: ${({ theme }) => theme.palette.secondary.main};
`;

const ListWrapper = styled(Box)(
  ({ theme }) => `
        .MuiTouchRipple-root {
            display: none;
        }
        
        .MuiListItem-root {
            transition: ${theme.transitions.create(['color', 'fill'])};
            
            &.MuiListItem-indicators {
                padding: ${theme.spacing(1, 2)};
            
                .MuiListItemText-root {
                    .MuiTypography-root {
                        &:before {
                            height: 4px;
                            width: 22px;
                            opacity: 0;
                            visibility: hidden;
                            display: block;
                            position: absolute;
                            bottom: -10px;
                            transition: all .2s;
                            border-radius: ${theme.general.borderRadiusLg};
                            content: "";
                            background: ${theme.colors.primary.main};
                        }
                    }
                }

                &.active,
                &:active,
                &:hover {
                
                    background: transparent;
                
                    .MuiListItemText-root {
                        .MuiTypography-root {
                            &:before {
                                opacity: 1;
                                visibility: visible;
                                bottom: 0px;
                            }
                        }
                    }
                }
            }
        }
`
);
