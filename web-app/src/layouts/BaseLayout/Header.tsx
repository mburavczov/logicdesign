import { Box, styled } from '@mui/material';
import { HeaderMenu } from '@/layouts/BaseLayout/HeaderMenu';
import { HeaderUserbox } from '@/layouts/BaseLayout/HeaderUserbox';
import { Logo } from '@/components';

export function Header() {
  return (
    <HeaderWrapper display="flex" alignItems="center">
      <Box display="flex" alignItems="center" gap={1}>
        <Logo />
        <Box
          component="span"
          sx={{
            display: { xs: 'none', md: 'inline-block' }
          }}
        >
          <HeaderMenu />
        </Box>
      </Box>
      <Box display="flex" alignItems="center">
        <HeaderUserbox />
        <Box
          component="span"
          sx={{
            display: { md: 'none', xs: 'inline-block' }
          }}
        ></Box>
      </Box>
    </HeaderWrapper>
  );
}

const HeaderWrapper = styled(Box)(
  ({ theme }) => `
    height: ${theme.header.height};
    color: ${theme.header.textColor};
    padding: ${theme.spacing(0, 2)};
    right: 0;
    top: 0;
    z-index: 6;
    background-color: ${theme.header.background};
    box-shadow: ${theme.header.boxShadow};
    position: fixed;
    justify-content: space-between;
    width: 100%;
`
);
