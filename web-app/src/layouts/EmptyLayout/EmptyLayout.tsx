import { ReactNode } from 'react';
import { Box } from '@mui/material';

interface EmptyLayoutProps {
  children?: ReactNode;
}

export function EmptyLayout({ children }: EmptyLayoutProps) {
  return (
    <Box
      sx={{
        display: 'flex',
        flex: 1,
        height: '100%'
      }}
    >
      {children}
    </Box>
  );
}
