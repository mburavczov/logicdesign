import { useMutation, UseMutationOptions } from 'react-query';
import { client } from '@/api';

interface DeleteMemberToDocumentRequest {
  documentId: string;
  personId: number;
}

export function useDeleteMemberToDocumentMutation(
  options: Pick<
    UseMutationOptions<unknown, unknown, DeleteMemberToDocumentRequest>,
    'onError' | 'onSuccess'
  >
) {
  const mutation = useMutation<unknown, unknown, DeleteMemberToDocumentRequest>(
    (data) => client.post('Document/DeleteMember', data),
    options
  );

  return mutation.mutateAsync;
}
