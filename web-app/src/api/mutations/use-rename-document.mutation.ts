import { useMutation, UseMutationOptions } from 'react-query';
import { client } from '@/api';

interface RenameDocumentRequest {
  documentId: string;
  name: string;
}

export function useRenameDocumentMutation(
  options: Pick<
    UseMutationOptions<unknown, unknown, RenameDocumentRequest>,
    'onError' | 'onSuccess'
  >
) {
  const mutation = useMutation<unknown, unknown, RenameDocumentRequest>(
    (data) => client.post('Document/Rename', data),
    options
  );

  return mutation.mutateAsync;
}
