import { useMutation, UseMutationOptions } from 'react-query';
import { client } from '@/api';

interface AddMemberToDocumentRequest {
  documentId: string;
  personId: number;
}

export function useAddMemberToDocumentMutation(
  options: Pick<
    UseMutationOptions<unknown, unknown, AddMemberToDocumentRequest>,
    'onError' | 'onSuccess'
  >
) {
  const mutation = useMutation<unknown, unknown, AddMemberToDocumentRequest>(
    (data) => client.post('Document/AddMember', data),
    options
  );

  return mutation.mutateAsync;
}
