import { useMutation, UseMutationOptions } from 'react-query';
import { client } from '@/api';

interface DeleteDocumentRequest {
  documentId: string;
}

export function useDeleteDocumentMutation(
  options: Pick<
    UseMutationOptions<unknown, unknown, DeleteDocumentRequest>,
    'onError' | 'onSuccess'
  >
) {
  const mutation = useMutation<unknown, unknown, DeleteDocumentRequest>(
    (data) => client.post('Document/Delete', data),
    options
  );

  return mutation.mutateAsync;
}
