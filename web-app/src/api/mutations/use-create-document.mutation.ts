import { useMutation, UseMutationOptions } from 'react-query';
import { client } from '@/api';
import { Document } from '@/models/document';

interface CreateDocumentRequest {
  name?: string;
}

export function useCreateDocumentMutation(
  options: Pick<
    UseMutationOptions<Document, unknown, CreateDocumentRequest>,
    'onError' | 'onSuccess'
  >
) {
  const mutation = useMutation<Document, unknown, CreateDocumentRequest>(
    async (data) => (await client.post('Document/Create', data)).data,
    options
  );

  return mutation.mutateAsync;
}
