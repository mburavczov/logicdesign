import { useMutation, UseMutationOptions } from 'react-query';
import { client } from '@/api';

interface UpdateDocumentRequest {
  documentId: string;
  settings: string;
}

export function useUpdateDocumentMutation(
  options: Pick<
    UseMutationOptions<unknown, unknown, UpdateDocumentRequest>,
    'onError' | 'onSuccess'
  >
) {
  const mutation = useMutation<unknown, unknown, UpdateDocumentRequest>(
    (data) => client.post('Document/Update', data),
    options
  );

  return mutation.mutateAsync;
}
