import { useQuery } from 'react-query';
import { client } from '@/api';
import { Document } from '@/models/document';

export function useAllDocumentsQuery() {
  return useQuery<Document[]>(
    ['all-documents'],
    async () => {
      try {
        const dto = await client.get('Document/GetMy');
        return dto.data;
      } catch {
        console.log('fatal error');
      }
    },
    {}
  );
}
