import { useQuery } from 'react-query';
import { client } from '@/api';
import { User } from '@/models';

export function useUsersQuery() {
  return useQuery<User[]>(
    ['users'],
    async () => {
      try {
        const dto = await client.get('Profile/Persons');
        return dto.data;
      } catch {
        console.log('fatal error');
      }
    },
    {}
  );
}
