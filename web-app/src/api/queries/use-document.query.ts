import { useQuery } from 'react-query';
import { client } from '@/api';
import { Document } from '@/models/document';

export function useDocumentQuery(documentId: string) {
  return useQuery<Document>(
    ['document', documentId],
    async () => {
      try {
        const dto = await client.get('Document/Get', {
          params: { documentId }
        });
        return dto.data;
      } catch {
        console.log('fatal error');
      }
    },
    {}
  );
}
