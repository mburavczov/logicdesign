import { User } from '@/models/user';

export interface Document {
  id: string;
  name: string;
  ownerId: number;
  owner: User;
  settings: string;
  createdAt: Date | string;
  updatedAt: Date | string;
  deletedAt: Date | string;
  members: User[];
}
