export interface User {
  id: number;
  login: string;
  role: string;
  state: string;
  name: string;
  surname: string;
}
