export interface LoginRequest {
  login: string;
  pass: string;
  documentId?: string;
}

export interface LoginResponse {
  token: string;
}
