export interface RegisterRequest {
  name: string;
  surname: string;
  login: string;
  pass: string;
  documentId?: string;
}

export interface RegisterResponse {
  token: string;
}
