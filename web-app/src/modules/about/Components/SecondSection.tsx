import React from 'react';
import styled from '@emotion/styled';
import Image from 'next/image';
import TextCard from '@/modules/about/Components/TextCard';

const SecondSection = () => {
  const text =
    'TulaHack Традиционные 40 часов работы над IT-проектом на заданную работодателями тему. В этом сезоне с нами постоянные компании-партнеры и полюбившиеся эксперты, а также мы рады новым партнерам!/Accelerator> Хакатон проводится в рамках акселератора ТулГУ "inTECH" по гранту АНО «Национальная технологическая инициатива» в целях организации акселерационных программ поддержки проектных команд и студенческих инициатив для формирования инновационных продуктов.';
  return (
    <>
      <SectionWrapper>
        <Image src="/static/about/Laptop.svg" width={830} height={526}></Image>
        <TextCard title={'Logic Design'} text={text}></TextCard>
      </SectionWrapper>
    </>
  );
};

const SectionWrapper = styled.div({
  width: '1450px',
  margin: '0 auto 300px',
  display: 'flex',
  alignItems: 'center'
});

export default SecondSection;
