import React from 'react';
import styled from '@emotion/styled';

const TextCard = ({ title, text }) => {
  return (
    <>
      <Section>
        <Title>{title}</Title>
        <Text>{text}</Text>
      </Section>
    </>
  );
};

const Section = styled.div({
  width: '615px'
});

const Title = styled.text({
  fontFamily: 'Gilroy',
  fontStyle: 'normal',
  fontWeight: '800',
  fontSize: '60px',
  color: '#3E4755'
});

const Text = styled.div({
  fontSize: '16px',
  color: '#7B8393',
  marginTop: '25px',
  maxWidth: '500px'
});

export default TextCard;
