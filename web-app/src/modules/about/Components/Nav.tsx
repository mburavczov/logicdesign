import React from 'react';
import Image from 'next/image';
import styled from '@emotion/styled';
import Button from '@/modules/about/Components/Button';
import { useRouter } from 'next/router';
import { useAuth } from '@/hooks';

const Nav = () => {
  const { push } = useRouter();
  const { isAuthenticated } = useAuth();

  return (
    <>
      <NavWrapper>
        <Image src="/static/logo.svg" width={56} height={40}></Image>
        <Menu>
          <Button
            text="Создать схему"
            onClick={() => {
              push('/flowcharts');
            }}
          />
          <Button
            text={isAuthenticated ? 'Мои схемы' : 'Войти'}
            onClick={() => {
              push(isAuthenticated ? '/dashboard' : '/sign-in');
            }}
          ></Button>
        </Menu>
      </NavWrapper>
    </>
  );
};

const NavWrapper = styled.div({
  width: '1450px',
  display: 'flex',
  justifyContent: 'space-between',
  margin: '0 auto',
  padding: '40px 0'
});

const Menu = styled.div({
  display: 'flex'
});

export default Nav;
