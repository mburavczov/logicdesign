import React from 'react';
import Image from 'next/image';
import styled from '@emotion/styled';
import TextCard from '@/modules/about/Components/TextCard';

const MainSection = () => {
  return (
    <>
      <MainSectionWrapper>
        <ImageWrapper>
          <Image
            src="/static/about/BigLogo.svg"
            width={560}
            height={415}
          ></Image>
        </ImageWrapper>
        <TextCardWrapper>
          <TextCard
            title={'Logic Design'}
            text="Сервис создания и совместного редактирования диаграмм и блок-схем. У нас Вы можете создать схему и редактировать её вместе со своей командой."
          ></TextCard>
        </TextCardWrapper>
      </MainSectionWrapper>
    </>
  );
};

const TextCardWrapper = styled.div({
  position: 'relative',
  top: '-200px'
});

const MainSectionWrapper = styled.div({
  width: '1450px',
  margin: '50px auto 500px'
});

const ImageWrapper = styled.div({
  position: 'relative',
  left: '-100px'
});

export default MainSection;
