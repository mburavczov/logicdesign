import React from 'react';
import NumberCard from '@/modules/about/Components/NumberCard';
import styled from '@emotion/styled';

const TirdSection = () => {
  const item = [1, 2, 3];
  const text =
    'TulaHack Традиционные 40 часов работы над IT-проектом на заданную работодателями тему. В этом сезоне с нами постоянные компании-партнеры и полюбившиеся эксперты, а также мы рады новым партнерам!/Accelerator> Хакатон';

  return (
    <>
      <CardContainer style={{ margin: '0 auto' }}>
        {item.map((item) => (
          <NumberCard number={item} text={text} />
        ))}
      </CardContainer>
    </>
  );
};

const CardContainer = styled.div({
  width: '1450px',
  display: 'flex',
  justifyContent: 'space-between',
  margin: '0 auto 330px'
});

export default TirdSection;
