import React from 'react';
import Image from 'next/image';
import styled from '@emotion/styled';

const FourthSection = () => {
  return (
    <>
      <ImageWrapper>
        <Image src="/static/about/Laptop.svg" width={1300} height={800}></Image>
      </ImageWrapper>
    </>
  );
};

const ImageWrapper = styled.div({
  margin: '0 auto 400px',
  width: '1450px'
});

export default FourthSection;
