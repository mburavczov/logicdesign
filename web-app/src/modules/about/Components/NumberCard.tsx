import React from 'react';
import styled from '@emotion/styled';

const NumberCard = ({ number, text }) => {
  return (
    <>
      <Section>
        <TypographyCenter>
          <Number>{number}</Number>
        </TypographyCenter>
        <TypographyCenter>
          <Typography style={{ position: 'relative', top: '-120px' }}>
            {text}
          </Typography>
        </TypographyCenter>
      </Section>
    </>
  );
};

const Section = styled.div({
  width: '415px',
  userSelect: 'none'
});

const Typography = styled.text({
  fontFamily: 'Gilroy',
  fontSize: '16px',
  fontStyle: 'normal',
  color: '#7B8393'
});

const Number = styled.text({
  fontSize: '410px',
  color: '#7B8393',
  opacity: '0.2'
});

const TypographyCenter = styled.div({
  textAlign: 'center'
});

export default NumberCard;
