import React from 'react';
import { styled } from '@mui/material';

const Button = ({ text, onClick }) => {
  return <Btn onClick={onClick}>{text}</Btn>;
};

const Btn = styled('div')`
  font-size: 16px;
  line-height: 157%;
  color: #ffffff;
  border-radius: 40px;
  cursor: pointer;
  padding: 0.5rem 2rem;
  background: rgba(255, 255, 255, 0.2);
  border: none;
  z-index: 100;
  margin-right: 30px;

  :hover {
    background: rgba(255, 255, 255, 0.35);
  }
`;

export default Button;
