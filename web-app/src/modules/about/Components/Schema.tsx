import React from 'react';
import styled from '@emotion/styled';

const Schema = ({ text, schemaType }) => {
  {
    if (schemaType === 1) {
      return (
        <Line>
          <Text>{text}</Text>
        </Line>
      );
    } else {
      return <Dottet>{text}</Dottet>;
    }
  }
};

const Text = styled.text({});

const Dottet = styled.div({
  height: '322px',
  width: '439px',
  fontSize: '50px',
  fontFamily: 'inter',
  textAlign: 'center',
  padding: '2.5em 0',
  borderRadius: '40px',
  border: '4px dashed #98A7F3'
});

const Line = styled.div({
  background: 'rgba(93,112,255,0.2)',
  height: '322px',
  width: '439px',
  fontSize: '50px',
  fontFamily: 'inter',
  textAlign: 'center',
  padding: '2.5em 0',
  borderRadius: '40px'
});

export default Schema;
