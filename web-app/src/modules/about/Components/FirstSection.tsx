import React from 'react';
import Schema from '@/modules/about/Components/Schema';
import TextCard from '@/modules/about/Components/TextCard';
import styled from '@emotion/styled';

const FirstSection = () => {
  const text =
    'TulaHack Традиционные 40 часов работы над IT-проектом на заданную работодателями тему. В этом сезоне с нами постоянные компании-партнеры и полюбившиеся эксперты, а также мы рады новым партнерам!Accelerator Хакатон проводится в рамках акселератора ТулГУ "inTECH" по гранту АНО «Национальная технологическая инициатива» в целях организации акселерационных программ поддержки проектных команд и студенческих инициатив для формирования инновационных продуктов.';
  return (
    <>
      <SectionWrapper>
        <TextCard title={'Logic Design'} text={text}></TextCard>
        <SchemaWrapper>
          <Schema text={'block'} schemaType={2}></Schema>
        </SchemaWrapper>
        <SchemaWr>
          <Schema text={'schema'} schemaType={1}></Schema>
        </SchemaWr>
      </SectionWrapper>
    </>
  );
};

const SchemaWr = styled.div({
  position: 'relative',
  top: '100px',
  left: '100px'
});
const SchemaWrapper = styled.div({
  position: 'relative',
  top: '-100px',
  left: '200px'
});

const SectionWrapper = styled.div({
  width: '1450px',
  margin: '0 auto 350px',
  display: 'flex',
  justifyContent: 'space-between'
});

export default FirstSection;
