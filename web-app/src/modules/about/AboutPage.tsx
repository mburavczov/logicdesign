import { Stack } from '@mui/material';
import styled from '@emotion/styled';
import Footer from '@/modules/about/Components/Footer';
import Nav from '@/modules/about/Components/Nav';
import MainSection from '@/modules/about/Components/MainSection';
import Image from 'next/image';
import React from 'react';

export function AboutPage() {
  return (
    <Stack
      style={{ padding: '0', maxHeight: '100%', overflow: 'hidden' }}
      gap={1}
      p={10}
    >
      <div>
        <ImageWrapper>
          <Image
            src="/static/about/picture.svg"
            width={1390}
            height={1500}
          ></Image>
        </ImageWrapper>
        <Nav />
        <MainSection />
        <Footer />
      </div>
    </Stack>
  );
}

const ImageWrapper = styled.div({
  position: 'absolute',
  top: '0',
  left: '660px'
});
