import {
  Avatar,
  Box,
  Card,
  CardContent,
  CardActionArea,
  Container,
  Grid,
  styled,
  Tooltip,
  Typography
} from '@mui/material';
import AddTwoToneIcon from '@mui/icons-material/AddTwoTone';
import React from 'react';
import { DashboardCard } from '@/modules/dashboard/DashboardCard';
import { Link } from '@/components';
import { useAllDocumentsQuery } from '@/api';

export function DashboardPage() {
  const { data: documents = [] } = useAllDocumentsQuery();

  return (
    <Container>
      <Box
        display="flex"
        alignItems="center"
        justifyContent="space-between"
        sx={{
          py: 5
        }}
      >
        <Typography variant="h3">Мои схемы</Typography>
      </Box>

      <Grid container spacing={4}>
        <Grid item xs={12} md={3}>
          <Tooltip arrow title="Создать новую схему">
            <Link href="/flowcharts">
              <CardAddAction>
                <CardActionArea
                  sx={{
                    px: 1
                  }}
                >
                  <CardContent>
                    <AvatarAddWrapper>
                      <AddTwoToneIcon fontSize="large" />
                    </AvatarAddWrapper>
                  </CardContent>
                </CardActionArea>
              </CardAddAction>
            </Link>
          </Tooltip>
        </Grid>

        {documents.map((document) => (
          <Grid item xs={12} md={3} key={document.id}>
            <DashboardCard document={document} />
          </Grid>
        ))}
      </Grid>
    </Container>
  );
}

const CardAddAction = styled(Card)(
  ({ theme }) => `
        border: ${theme.colors.primary.main} dashed 1px;
        height: 260px;
        color: ${theme.colors.primary.main};
        transition: ${theme.transitions.create(['all'])};
        
        .MuiCardActionArea-root {
          height: 100%;
          justify-content: center;
          align-items: center;
          display: flex;
        }
        
        .MuiTouchRipple-root {
          opacity: .2;
        }
        
        &:hover {
          border-color: ${theme.colors.primary.main};
        }
`
);

const AvatarAddWrapper = styled(Avatar)(
  ({ theme }) => `
    background: ${theme.colors.alpha.black[10]};
    color: ${theme.colors.primary.main};
    width: ${theme.spacing(8)};
    height: ${theme.spacing(8)};
`
);
