import {
  alpha,
  Box,
  Button,
  Card,
  CardContent,
  lighten,
  styled,
  useTheme
} from '@mui/material';
import React from 'react';
import { Document } from '@/models/document';
import { Link } from '@/components';

interface DashboardCardProps {
  document: Document;
}

export function DashboardCard({ document }: DashboardCardProps) {
  const theme = useTheme();

  return (
    <Card
      sx={{
        textAlign: 'center',
        position: 'relative',
        transition: `${theme.transitions.create(['box-shadow', 'transform'])}`,
        transform: 'translateY(0px)',

        '&:hover': {
          transform: `translateY(-${theme.spacing(0.5)})`,
          boxShadow: `0 2rem 8rem 0 ${alpha(
            theme.colors.alpha.black[100],
            0.05
          )}, 
          0 0.6rem 1.6rem ${alpha(theme.colors.alpha.black[100], 0.15)}, 
          0 0.2rem 0.2rem ${alpha(theme.colors.alpha.black[100], 0.1)}`,

          '& .MuiBgComposed': {
            opacity: 1
          }
        }
      }}
    >
      <BgComposed
        display="flex"
        flexDirection="column"
        gap={1}
        alignItems="center"
        justifyContent="center"
        className="MuiBgComposed"
      >
        <Button
          component={Link}
          href={`/flowcharts/${document.id}`}
          sx={{
            px: 2.5,
            borderRadius: 10,
            transform: 'scale(1)',
            transition: `${theme.transitions.create(['all'])}`,
            boxShadow: `${theme.colors.shadows.primary}`,

            '&:hover': {
              transform: 'scale(1.05)',
              boxShadow: `${theme.colors.shadows.primary}`
            },
            '&:active': {
              boxShadow: 'none'
            }
          }}
          variant="contained"
          color="primary"
        >
          Посмотреть
        </Button>
      </BgComposed>
      <CardContent
        sx={{
          borderRadius: 'inherit',
          position: 'relative',
          zIndex: 5,
          height: 260
        }}
      >
        {document.name}
      </CardContent>
      <CardActions
        sx={{
          bottom: `${theme.spacing(2)}`,
          top: 'auto',
          right: 'auto',
          left: `${theme.spacing(2)}`
        }}
      >
        <Label
          sx={{
            background: `${theme.palette.primary.main}`,
            color: `${theme.palette.primary.contrastText}`
          }}
        >
          {document.name}
        </Label>
      </CardActions>
    </Card>
  );
}

const BgComposed = styled(Box)(
  ({ theme }) => `
    position: absolute;
    width: 100%;
    height: 100%;
    left: 0;
    top: 0;
    transition: ${theme.transitions.create(['opacity'])};
    background: ${alpha(lighten(theme.palette.common.black, 0.9), 0.5)};
    z-index: 6;
    opacity: 0;
  `
);

const CardActions = styled(Box)(
  ({ theme }) => `
    position: absolute;
    right: ${theme.spacing(2)};
    bottom: ${theme.spacing(2)};
    z-index: 7;
  `
);

const Label = styled(Box)(
  ({ theme }) => `
    background: ${theme.palette.success.main};
    color: ${theme.palette.success.contrastText};
    text-transform: uppercase;
    font-size: ${theme.typography.pxToRem(10)};
    font-weight: bold;
    line-height: 23px;
    height: 22px;
    padding: ${theme.spacing(0, 1.2)};
    border-radius: 50px;
  `
);
