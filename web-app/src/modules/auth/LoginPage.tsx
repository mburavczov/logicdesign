import { CenterContent, Link, Logo } from '@/components';
import { Box, Typography, Card } from '@mui/material';
import { LoginForm } from '@/modules/auth/LoginForm';
import { useRouter } from 'next/router';

export function LoginPage() {
  const router = useRouter();

  return (
    <CenterContent>
      <Box textAlign="center">
        <Logo />
      </Box>
      <Card
        sx={{
          p: 4,
          my: 4
        }}
      >
        <Box textAlign="center">
          <Typography
            variant="h2"
            sx={{
              mb: 1
            }}
          >
            Вход
          </Typography>
          <Typography
            variant="h4"
            color="text.secondary"
            fontWeight="normal"
            sx={{
              mb: 3
            }}
          >
            Заполните поля ниже, чтобы войти в свою учетную запись.
          </Typography>
        </Box>
        <LoginForm />
        <Box my={4}>
          <Typography
            component="span"
            variant="subtitle2"
            color="text.primary"
            fontWeight="bold"
          >
            Еще нет учетной записи?
          </Typography>{' '}
          <Link
            href={
              router.query.documentId
                ? `/sign-up?documentId=${router.query.documentId}`
                : '/sign-up'
            }
          >
            <b>Регистрация</b>
          </Link>
        </Box>
      </Card>
    </CenterContent>
  );
}
