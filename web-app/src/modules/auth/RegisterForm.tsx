import { Button, CircularProgress, TextField } from '@mui/material';
import { useAuth, useRefMounted } from '@/hooks';
import { useRouter } from 'next/router';
import { useFormik } from 'formik';
import * as Yup from 'yup';

export function RegisterForm() {
  const { register } = useAuth();
  const isMountedRef = useRefMounted();
  const router = useRouter();

  const formik = useFormik({
    initialValues: {
      name: '',
      surname: '',
      login: '',
      pass: '',
      submit: null,
      documentId: router.query.documentId as string | undefined
    },
    validationSchema: Yup.object({
      name: Yup.string().required('Не введено имя.'),
      surname: Yup.string().required('Не введена фамилия.'),
      login: Yup.string()
        .email('Введен некорректный Email.')
        .max(255)
        .required('Не введен Email.'),
      pass: Yup.string()
        .min(8, 'Должно быть не меньше 8 символов.')
        .max(255)
        .required('Не введен пароль.'),
      submit: null
    }),
    onSubmit: async (values, helpers): Promise<void> => {
      try {
        await register(values);

        if (isMountedRef()) {
          const backTo = (router.query.backTo as string) || '/dashboard';
          router.replace(backTo);
        }
      } catch (err) {
        console.error(err);
        if (isMountedRef()) {
          helpers.setStatus({ success: false });
          helpers.setErrors({
            submit: 'Некорректная регистрация.'
          });
          helpers.setSubmitting(false);
        }
      }
    }
  });

  return (
    <form noValidate onSubmit={formik.handleSubmit}>
      <TextField
        error={Boolean(formik.touched.name && formik.errors.name)}
        helperText={formik.touched.name && formik.errors.name}
        fullWidth
        margin="normal"
        label="Имя"
        name="name"
        onBlur={formik.handleBlur}
        onChange={formik.handleChange}
        type="text"
        value={formik.values.name}
        variant="outlined"
      />
      <TextField
        error={Boolean(formik.touched.surname && formik.errors.surname)}
        helperText={formik.touched.surname && formik.errors.surname}
        fullWidth
        margin="normal"
        label="Фамилия"
        name="surname"
        onBlur={formik.handleBlur}
        onChange={formik.handleChange}
        type="text"
        value={formik.values.surname}
        variant="outlined"
      />
      <TextField
        error={Boolean(formik.touched.login && formik.errors.login)}
        helperText={formik.touched.login && formik.errors.login}
        fullWidth
        margin="normal"
        label="Email"
        name="login"
        onBlur={formik.handleBlur}
        onChange={formik.handleChange}
        type="email"
        value={formik.values.login}
        variant="outlined"
      />
      <TextField
        error={Boolean(formik.touched.pass && formik.errors.pass)}
        helperText={formik.touched.pass && formik.errors.pass}
        fullWidth
        margin="normal"
        label="Пароль"
        name="pass"
        onBlur={formik.handleBlur}
        onChange={formik.handleChange}
        type="password"
        value={formik.values.pass}
        variant="outlined"
      />

      <Button
        sx={{
          mt: 3
        }}
        color="primary"
        startIcon={
          formik.isSubmitting ? <CircularProgress size="1rem" /> : null
        }
        disabled={formik.isSubmitting}
        type="submit"
        fullWidth
        size="large"
        variant="contained"
      >
        Зарегистрироваться
      </Button>
    </form>
  );
}
