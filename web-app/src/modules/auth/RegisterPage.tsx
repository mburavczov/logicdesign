import { CenterContent, Link, Logo } from '@/components';
import { Box, Card, Typography } from '@mui/material';
import { RegisterForm } from '@/modules/auth/RegisterForm';
import { useRouter } from 'next/router';

export function RegisterPage() {
  const router = useRouter();

  return (
    <CenterContent>
      <Box textAlign="center">
        <Logo />
      </Box>
      <Card
        sx={{
          p: 4,
          my: 4
        }}
      >
        <Box textAlign="center">
          <Typography
            variant="h2"
            sx={{
              mb: 1
            }}
          >
            Регистрация
          </Typography>
          <Typography
            variant="h4"
            color="text.secondary"
            fontWeight="normal"
            sx={{
              mb: 3
            }}
          >
            Заполните поля ниже, чтобы создать учетную запись.
          </Typography>
        </Box>
        <RegisterForm />
        <Box my={4}>
          <Typography
            component="span"
            variant="subtitle2"
            color="text.primary"
            fontWeight="bold"
          >
            Уже есть учетная запись?
          </Typography>{' '}
          <Link
            href={
              router.query.documentId
                ? `/sign-in?documentId=${router.query.documentId}`
                : '/sign-in'
            }
          >
            <b>Вход</b>
          </Link>
        </Box>
      </Card>
    </CenterContent>
  );
}
