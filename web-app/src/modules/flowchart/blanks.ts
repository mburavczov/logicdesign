import { css, styled } from '@mui/material';

const base = css`
  width: 100px;
  height: 50px;
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const Begin = styled('div')`
  border: 1px solid ${({ theme }) => theme.palette.primary.main};
  ${base};

  border-radius: 100px/50px;
`;

export const End = styled('div')`
  border: 1px solid ${({ theme }) => theme.palette.primary.main};
  ${base};

  border-radius: 100px/50px;
`;

export const Action = styled('div')`
  border: 1px solid ${({ theme }) => theme.palette.primary.main};
  ${base};

  border-radius: 10px;
`;

export const Condition = styled('div')`
  border: 1px solid ${({ theme }) => theme.palette.primary.main};
  ${base};
  border-radius: 10px;
`;

export const Input = styled('div')`
  border: 1px solid ${({ theme }) => theme.palette.primary.main};
  ${base};

  border-radius: 10px;
  transform: skew(-20deg);
`;

export const Comment = styled('div')`
  border: 1px solid ${({ theme }) => theme.palette.primary.main};
  ${base};
`;
