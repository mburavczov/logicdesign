import { Loader } from '@/components';
import { useCreateDocumentMutation } from '@/api';
import { useEffect } from 'react';
import { useRouter } from 'next/router';

export function CreateFlowChartPage() {
  const { push } = useRouter();
  const createDocument = useCreateDocumentMutation({
    onSuccess: (document) => {
      push(`/flowcharts/${document.id}`);
    }
  });

  useEffect(() => {
    createDocument({ name: 'Новая схема' });
  }, []);

  return <Loader />;
}
