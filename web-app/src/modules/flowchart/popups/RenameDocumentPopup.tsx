import { useFormik } from 'formik';
import * as Yup from 'yup';
import { DialogPopup } from '@/components';
import {
  Button,
  CircularProgress,
  Slide,
  TextField,
  Typography
} from '@mui/material';
import { useFlowChart } from '@/modules/flowchart/FlowChartContext';
import { useRenameDocumentMutation } from '@/api';
import { useSnackbar } from 'notistack';

interface RenameDocumentPopupProps {
  open: boolean;
  toggleOpen: () => void;
}

export function RenameDocumentPopup({
  open,
  toggleOpen
}: RenameDocumentPopupProps) {
  const { enqueueSnackbar } = useSnackbar();
  const { document, refetch } = useFlowChart();
  const renameDocument = useRenameDocumentMutation({
    onSuccess: () => {
      toggleOpen();
      refetch();
      enqueueSnackbar('Схема успешно переименована!', {
        variant: 'success',
        anchorOrigin: {
          vertical: 'bottom',
          horizontal: 'right'
        },
        autoHideDuration: 2000,
        TransitionComponent: Slide
      });
    }
  });
  const formik = useFormik({
    initialValues: {
      documentId: document.id,
      name: document.name
    },
    validationSchema: Yup.object({
      name: Yup.string().required('Введите название схемы')
    }),
    onSubmit: async (values): Promise<void> => {
      await renameDocument(values);
    }
  });

  return (
    <>
      <DialogPopup
        header={
          <Typography variant="h4" gutterBottom>
            Изменение названия схемы
          </Typography>
        }
        actions={
          <>
            <Button color="secondary" onClick={toggleOpen}>
              Отмена
            </Button>
            <Button
              onClick={() => formik.handleSubmit()}
              type="submit"
              startIcon={
                formik.isSubmitting ? <CircularProgress size="1rem" /> : null
              }
              disabled={formik.isSubmitting}
              variant="contained"
            >
              Изменить
            </Button>
          </>
        }
        open={open}
        onClose={toggleOpen}
      >
        <TextField
          error={Boolean(formik.touched.name && formik.errors.name)}
          fullWidth
          margin="normal"
          helperText={formik.touched.name && formik.errors.name}
          label={'Название'}
          name="name"
          onBlur={formik.handleBlur}
          onChange={formik.handleChange}
          value={formik.values.name}
          variant="outlined"
        />
      </DialogPopup>
    </>
  );
}
