import { useSnackbar } from 'notistack';
import { useFlowChart } from '@/modules/flowchart/FlowChartContext';
import {
  Avatar,
  Box,
  Button,
  Divider,
  FormControl,
  Grid,
  IconButton,
  InputLabel,
  MenuItem,
  Select,
  Slide,
  styled,
  Typography
} from '@mui/material';
import { DialogPopup } from '@/components';
import { useAuth } from '@/hooks';
import {
  useAddMemberToDocumentMutation,
  useDeleteMemberToDocumentMutation,
  useUsersQuery
} from '@/api';
import { useState } from 'react';
import DeleteIcon from '@mui/icons-material/Delete';

interface CollaborationPopupProps {
  open: boolean;
  toggleOpen: () => void;
}

const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: 220
    }
  }
};

export function CollaborationPopup({
  open,
  toggleOpen
}: CollaborationPopupProps) {
  const { user, isAuthenticated } = useAuth();
  const { enqueueSnackbar } = useSnackbar();
  const { document, refetch } = useFlowChart();
  const { data: users = [] } = useUsersQuery();
  const [selectedPersonId, setSelectedPersonId] = useState<number>();

  const addMember = useAddMemberToDocumentMutation({
    onSuccess: () => {
      refetch();
      enqueueSnackbar('Пользователь успешно добавлен!', {
        variant: 'success',
        anchorOrigin: {
          vertical: 'bottom',
          horizontal: 'right'
        },
        autoHideDuration: 2000,
        TransitionComponent: Slide
      });
    }
  });
  const deleteMember = useDeleteMemberToDocumentMutation({
    onSuccess: () => {
      refetch();
      enqueueSnackbar('Пользователь успешно удален!', {
        variant: 'success',
        anchorOrigin: {
          vertical: 'bottom',
          horizontal: 'right'
        },
        autoHideDuration: 2000,
        TransitionComponent: Slide
      });
    }
  });

  if (!isAuthenticated) {
    return null;
  }

  return (
    <DialogPopup
      header={
        <Typography variant="h4" gutterBottom>
          Общий доступ
        </Typography>
      }
      actions={
        <Button color="secondary" onClick={toggleOpen}>
          Закрыть
        </Button>
      }
      open={open}
      onClose={toggleOpen}
    >
      <Box display="flex" flexDirection="column" gap={2}>
        <Typography variant="h4">Владелец</Typography>
        <Box display="flex">
          <Avatar variant="rounded" alt={document.owner.name} src="" />
          <Box component="span">
            <UserBoxText>
              <UserBoxLabel variant="body1">
                {document.owner.name} {document.owner.surname}
              </UserBoxLabel>
              <UserBoxDescription variant="body2">
                {document.owner.login}
              </UserBoxDescription>
            </UserBoxText>
          </Box>
        </Box>
        {document.members.length > 0 && (
          <>
            <Typography variant="h4">Участники</Typography>
            <Grid container spacing={2}>
              {document.members.map(({ name, surname, login, id }) => (
                <Grid item xs={12} md={6}>
                  <Box
                    display="flex"
                    justifyContent="space-between"
                    alignItems="center"
                    gap={1}
                  >
                    <Box display="flex">
                      <Avatar variant="rounded" alt={name} src="" />
                      <Box component="span">
                        <UserBoxText>
                          <UserBoxLabel variant="body1">
                            {name} {surname}
                          </UserBoxLabel>
                          <UserBoxDescription variant="body2">
                            {login}
                          </UserBoxDescription>
                        </UserBoxText>
                      </Box>
                    </Box>

                    {document.ownerId === user.id && (
                      <IconButton
                        color="error"
                        aria-label="delete"
                        onClick={() =>
                          deleteMember({
                            documentId: document.id,
                            personId: id
                          })
                        }
                      >
                        <DeleteIcon />
                      </IconButton>
                    )}
                  </Box>
                </Grid>
              ))}
            </Grid>
          </>
        )}
      </Box>

      {document.ownerId === user.id && (
        <>
          <Divider sx={{ my: 3 }} />
          <Box display="flex" gap={4}>
            <FormControl fullWidth>
              <InputLabel id="users-label">Пользователи</InputLabel>
              <Select
                MenuProps={MenuProps}
                value={selectedPersonId}
                labelId="users-label"
                label="Пользователи"
                onChange={(event) => {
                  setSelectedPersonId(event.target.value as number);
                }}
                fullWidth
              >
                {users
                  .filter(
                    ({ id: userId }) =>
                      document.ownerId !== userId &&
                      !document.members.some(({ id }) => id === userId)
                  )
                  .map(({ name, surname, id }) => (
                    <MenuItem value={id} key={id}>
                      {name} {surname}
                    </MenuItem>
                  ))}
              </Select>
            </FormControl>
            <Button
              variant="contained"
              fullWidth
              disabled={!selectedPersonId}
              onClick={() =>
                addMember({
                  documentId: document.id,
                  personId: selectedPersonId
                })
              }
            >
              Добавить
            </Button>
          </Box>
        </>
      )}
    </DialogPopup>
  );
}

const UserBoxText = styled(Box)(
  ({ theme }) => `
        text-align: left;
        padding-left: ${theme.spacing(1)};
`
);

const UserBoxLabel = styled(Typography)(
  ({ theme }) => `
        font-weight: ${theme.typography.fontWeightBold};
        color: ${theme.palette.secondary.main};
        display: block;
`
);

const UserBoxDescription = styled(Typography)(
  ({ theme }) => `
        color: ${theme.palette.secondary.light}
`
);
