import { DialogPopup } from '@/components';
import { Button, Slide, Typography } from '@mui/material';
import { useFlowChart } from '@/modules/flowchart/FlowChartContext';
import { useDeleteDocumentMutation } from '@/api';
import { useSnackbar } from 'notistack';
import { useRouter } from 'next/router';

interface DeleteDocumentPopupProps {
  open: boolean;
  toggleOpen: () => void;
}

export function DeleteDocumentPopup({
  open,
  toggleOpen
}: DeleteDocumentPopupProps) {
  const { push } = useRouter();
  const { enqueueSnackbar } = useSnackbar();
  const { document } = useFlowChart();
  const deleteDocument = useDeleteDocumentMutation({
    onSuccess: () => {
      push('/dashboard');
      toggleOpen();
      enqueueSnackbar('Схема успешно удалена!', {
        variant: 'success',
        anchorOrigin: {
          vertical: 'bottom',
          horizontal: 'right'
        },
        autoHideDuration: 2000,
        TransitionComponent: Slide
      });
    }
  });

  return (
    <>
      <DialogPopup
        header={
          <Typography variant="h4" gutterBottom>
            Удаление схемы
          </Typography>
        }
        actions={
          <>
            <Button color="secondary" onClick={toggleOpen}>
              Отмена
            </Button>
            <Button
              onClick={() => deleteDocument({ documentId: document.id })}
              variant="contained"
              color="error"
            >
              Удалить
            </Button>
          </>
        }
        open={open}
        onClose={toggleOpen}
      >
        <Typography>Вы действительно хотите удалить схему?</Typography>
      </DialogPopup>
    </>
  );
}
