import { ReactFlowProvider } from 'react-flow-renderer';
import React from 'react';
import { styled } from '@mui/material';
import { FlowChartHeader } from '@/modules/flowchart/FlowChartHeader';
import { FlowChartSidebar } from '@/modules/flowchart/FlowChartSidebar';
import { FlowChartWorkArea } from '@/modules/flowchart/FlowChartWorkArea';
import { FlowChartProvider } from '@/modules/flowchart/FlowChartContext';

export function FlowChartPage() {
  return (
    <ReactFlowProvider>
      <FlowChartProvider>
        <Container>
          <FlowChartHeader />
          <Spacing />
          <Wrapper>
            <FlowChartSidebar />
            <FlowChartWorkArea />
          </Wrapper>
        </Container>
      </FlowChartProvider>
    </ReactFlowProvider>
  );
}

const Container = styled('div')`
  width: 100%;
  height: 100%;
`;

const Spacing = styled('div')`
  height: ${({ theme }) => theme.header.height};
`;

const Wrapper = styled('div')`
  display: flex;
  height: calc(100% - ${({ theme }) => theme.header.height});
  width: 100%;
`;
