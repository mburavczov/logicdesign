import React, { memo } from 'react';
import { Handle, NodeProps, Position } from 'react-flow-renderer';
import { useTheme } from '@mui/material';

export const CommentNode = memo(({ data, isConnectable }: NodeProps) => {
  const theme = useTheme();

  return (
    <>
      <Handle
        type="target"
        position={Position.Bottom}
        style={{ background: theme.palette.primary.main }}
        onConnect={(params) => console.log('handle onConnect', params)}
        isConnectable={isConnectable}
      />
      Начало
    </>
  );
});
