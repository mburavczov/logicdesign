import React, { memo } from 'react';
import { Handle, NodeProps, Position } from 'react-flow-renderer';
import { useTheme } from '@mui/material';

export const InputNode = memo(({ data, isConnectable }: NodeProps) => {
  const theme = useTheme();

  return (
    <>
      <Handle
        type="source"
        position={Position.Bottom}
        style={{ background: theme.palette.primary.main }}
        id="a"
        isConnectable={isConnectable}
      />
      Начало
    </>
  );
});
