import React, { memo } from 'react';
import { Handle, NodeProps, Position } from 'react-flow-renderer';
import { TextField, useTheme } from '@mui/material';
import { useFlowChart } from '@/modules/flowchart/FlowChartContext';
import { useBooleanState } from '@/hooks';

export const EndNode = memo(({ data, isConnectable }: NodeProps) => {
  const { nodesMap } = useFlowChart();
  const theme = useTheme();

  const [isEdit, toggleEdit] = useBooleanState();

  const changeName = (e) => {
    const currentNode = nodesMap.get(data.id);
    nodesMap.set(data.id, {
      ...currentNode,
      data: {
        ...currentNode.data,
        value: data.value,
        name: e.target.value
      }
    });
  };

  return (
    <>
      <Handle
        type="target"
        position={Position.Top}
        id="tE"
        style={{
          top: -8,
          background: theme.palette.primary.main,
          width: 16,
          height: 16
        }}
        isConnectable={isConnectable}
      />
      {isEdit ? (
        <TextField
          sx={{
            width: 120
          }}
          autoFocus
          onKeyDown={(e) => {
            if (e.code === 'Enter') {
              changeName(e);
              toggleEdit();
            }
          }}
          onBlur={(e) => {
            changeName(e);
            toggleEdit();
          }}
          defaultValue={data.name || 'Конец'}
          variant="standard"
          color="primary"
        />
      ) : (
        <span onDoubleClick={toggleEdit}>{data.name || 'Конец'}</span>
      )}
    </>
  );
});
