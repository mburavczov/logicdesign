import {
  createContext,
  useContext,
  ReactNode,
  useEffect,
  useState,
  useRef
} from 'react';
import { useRouterQueryId } from '@/hooks';
import { useDocumentQuery, useUpdateDocumentMutation } from '@/api';
import { Document } from '@/models/document';
import { Doc } from 'yjs';
import { WebrtcProvider } from 'y-webrtc';
import { Edge, Node } from 'react-flow-renderer';
import { YMap } from 'yjs/dist/src/types/YMap';
import { useSnackbar } from 'notistack';
import { Slide } from '@mui/material';

export type IFlowChartContext = {
  document: Document;
  saveSettings: () => void;
  refetch: () => void;
  nodesMap?: YMap<Node>;
  edgesMap?: YMap<Edge>;
};

export const FlowChartContext = createContext<IFlowChartContext | null>(null);

interface FlowChartProviderProps {
  children: ReactNode;
}

export function FlowChartProvider({ children }: FlowChartProviderProps) {
  const { enqueueSnackbar } = useSnackbar();
  const updateDocument = useUpdateDocumentMutation({
    onSuccess: () => {
      enqueueSnackbar('Схема успешно сохранена!', {
        variant: 'success',
        anchorOrigin: {
          vertical: 'bottom',
          horizontal: 'right'
        },
        autoHideDuration: 2000,
        TransitionComponent: Slide
      });
    }
  });

  const documentId = useRouterQueryId();
  const { data: document, refetch, isLoading } = useDocumentQuery(documentId);

  const [isMaps, setMaps] = useState(false);

  const nodesMap = useRef<YMap<Node>>();
  const edgesMap = useRef<YMap<Edge>>();
  const webrtc = useRef<WebrtcProvider>();

  useEffect(() => {
    if (document) {
      const yDoc = new Doc();

      webrtc.current = new WebrtcProvider(
        `logic-design-flow-editor-${document.id}`,
        yDoc
      );

      setTimeout(() => {
        if (webrtc.current.room.bcConns.size === 0 && document.settings) {
          const settings: { nodes: any[]; edges: any[] } = JSON.parse(
            document.settings
          );

          for (const key in settings.nodes) {
            nodesMap.current.set(key, settings.nodes[key]);
          }

          for (const key in settings.edges) {
            edgesMap.current.set(key, settings.edges[key]);
          }
        }
      }, 100);

      edgesMap.current = yDoc.getMap<Edge>('edges');
      nodesMap.current = yDoc.getMap<Node>('nodes');
      setMaps(true);

      return () => {
        webrtc.current.destroy();
      };
    }
  }, [isLoading]);

  if (!document || !isMaps) {
    return null;
  }

  const saveSettings = () => {
    const nodes = nodesMap.current.toJSON();
    const edges = edgesMap.current.toJSON();

    updateDocument({
      documentId: document.id,
      settings: JSON.stringify({ nodes, edges })
    });
  };

  return (
    <FlowChartContext.Provider
      value={{
        document,

        saveSettings,
        refetch,
        nodesMap: nodesMap.current,
        edgesMap: edgesMap.current
      }}
    >
      {children}
    </FlowChartContext.Provider>
  );
}

export function useFlowChart() {
  const context = useContext(FlowChartContext);

  if (context === null) {
    throw new Error(
      '`useWidgetModal` must be nested inside an `WidgetModalProvider`.'
    );
  }

  return context;
}
