import { useCallback, useEffect, useState } from 'react';
import {
  Edge,
  applyEdgeChanges,
  OnEdgesChange,
  OnConnect,
  Connection
} from 'react-flow-renderer';
import { useFlowChart } from '@/modules/flowchart/FlowChartContext';

function useNodesStateSynced(): [Edge[], OnEdgesChange, OnConnect] {
  const { edgesMap } = useFlowChart();
  const [edges, setEdges] = useState<Edge[]>([]);

  const onEdgesChange = useCallback((changes) => {
    const nextEdges = applyEdgeChanges(changes, Array.from(edgesMap.values()));
    changes.forEach((change) => {
      if (change.type !== 'remove') {
        edgesMap.set(
          change.id,
          nextEdges.find((n) => n.id === change.id)
        );
      } else {
        edgesMap.delete(change.id);
      }
    });
  }, []);

  const onConnect = useCallback((params: Connection | Edge) => {
    const { source, sourceHandle, target, targetHandle } = params;
    const id = `edge-${source}${sourceHandle || ''}-${target}${
      targetHandle || ''
    }`;
    edgesMap.set(id, {
      id,
      ...params
    });
  }, []);

  useEffect(() => {
    const observer = () => {
      setEdges(Array.from(edgesMap.values()));
    };

    setEdges(Array.from(edgesMap.values()));
    edgesMap.observe(observer);

    return () => edgesMap.unobserve(observer);
  }, [setEdges]);

  return [edges, onEdgesChange, onConnect];
}

export default useNodesStateSynced;
