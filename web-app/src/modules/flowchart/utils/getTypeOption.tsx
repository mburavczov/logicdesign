import React from 'react';
import { nodesMap } from '@/modules/flowchart/utils/useNodesStateSynced';

export const getTypeOptions = (type: string) => {
  let optionsArray: JSX.Element[] = [];
  const nodesArray = Array.from(nodesMap.values());
  const nodes = nodesArray.filter((node) => node.type === type);

  optionsArray.push(
    <option key={`${type}-node-none`} value="">
      -- Select a node type
    </option>
  );

  nodes.map((node) => {
    return optionsArray.push(
      <option key={`${type}-node-${node.data.id}`} value={node.data.value}>
        {node.data.name}
      </option>
    );
  });

  return optionsArray;
};
