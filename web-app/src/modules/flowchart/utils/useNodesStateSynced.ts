import { useCallback, useEffect, useState } from 'react';
import {
  applyNodeChanges,
  getConnectedEdges,
  Node,
  OnNodesChange
} from 'react-flow-renderer';
import { useFlowChart } from '@/modules/flowchart/FlowChartContext';

function useNodesStateSynced(): [Node[], OnNodesChange] {
  const { nodesMap, edgesMap } = useFlowChart();
  const [nodes, setNodes] = useState<Node[]>([]);

  const onNodesChanges = useCallback((changes) => {
    const nodes = Array.from(nodesMap.values());

    const nextNodes = applyNodeChanges(changes, nodes);
    changes.forEach((change) => {
      const node = nextNodes.find((n) => n.id === change.id);

      if (change.type !== 'remove') {
        nodesMap.set(change.id, node);
      } else if (change.type === 'remove') {
        nodesMap.delete(change.id);
        const edges = Array.from(edgesMap.values());
        const connectedEdges = getConnectedEdges(nodes, edges);
        connectedEdges.forEach((edge) => edgesMap.delete(edge.id));
      }
    });
  }, []);

  useEffect(() => {
    const observer = () => {
      setNodes(Array.from(nodesMap.values()));
    };

    setNodes(Array.from(nodesMap.values()));

    nodesMap.observe(observer);

    return () => nodesMap.unobserve(observer);
  }, [setNodes]);

  return [nodes, onNodesChanges];
}

export default useNodesStateSynced;
