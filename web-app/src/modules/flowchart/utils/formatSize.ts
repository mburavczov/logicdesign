export const formatSize = (input: any) => {
  let newValue = '';
  const incomingVal = input.toString();
  if (incomingVal) {
    if (
      incomingVal.includes('px') ||
      incomingVal.includes('em') ||
      incomingVal.includes('rem')
    ) {
      newValue = incomingVal;
    } else if (incomingVal.includes('.')) {
      // TODO look for base node in tokensinstead of using document root font size
      newValue = `${input}em`;
    } else {
      newValue = `${input}px`;
    }
  }
  return newValue;
};
