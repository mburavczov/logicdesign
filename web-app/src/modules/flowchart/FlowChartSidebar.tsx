import React, { DragEvent } from 'react';
import { Box, Divider, Grid, styled, Typography } from '@mui/material';
import { Action, Comment, End, Begin, Input, Condition } from './blanks';

const onDragStart = (event: DragEvent, nodeType: string) => {
  event.dataTransfer.setData('application/reactflow', nodeType);
  event.dataTransfer.effectAllowed = 'move';
};

export function FlowChartSidebar() {
  return (
    <SidebarWrapper>
      <Box display="flex" flexDirection="column" gap={3}>
        <Typography variant="h4" color="black">
          Блоки
        </Typography>
        <Grid container spacing={2}>
          <Grid item xs={12} md={6}>
            <Begin
              onDragStart={(event: DragEvent) =>
                onDragStart(event, 'beginNode')
              }
              draggable
            >
              Начало
            </Begin>
          </Grid>
          <Grid item xs={12} md={6}>
            <Action
              onDragStart={(event: DragEvent) =>
                onDragStart(event, 'actionNode')
              }
              draggable
            >
              Действие
            </Action>
          </Grid>
          <Grid item xs={12} md={6}>
            <Condition
              onDragStart={(event: DragEvent) =>
                onDragStart(event, 'conditionNode')
              }
              draggable
            >
              Условие
            </Condition>
          </Grid>
          {/*<Grid item xs={12} md={6}>*/}
          {/*  {' '}*/}
          {/*  <Input*/}
          {/*    onDragStart={(event: DragEvent) =>*/}
          {/*      onDragStart(event, 'inputNode')*/}
          {/*    }*/}
          {/*    draggable*/}
          {/*  >*/}
          {/*    Ввод/вывод*/}
          {/*  </Input>*/}
          {/*</Grid>*/}
          {/*<Grid item xs={12} md={6}>*/}
          {/*  <Comment*/}
          {/*    onDragStart={(event: DragEvent) =>*/}
          {/*      onDragStart(event, 'commentNode')*/}
          {/*    }*/}
          {/*    draggable*/}
          {/*  >*/}
          {/*    Комментарий*/}
          {/*  </Comment>*/}
          {/*</Grid>*/}
          <Grid item xs={12} md={6}>
            <End
              onDragStart={(event: DragEvent) => onDragStart(event, 'endNode')}
              draggable
            >
              Конец
            </End>
          </Grid>
        </Grid>
        <Divider />
        <Typography variant="h4" color="black">
          Подсказки
        </Typography>
        <Typography variant="body1" color="secondary">
          - Чтобы изменить название блока, кликните дважды на него
        </Typography>
        <Typography variant="body1" color="secondary">
          - Чтобы удалить блок, нажмите на него и кликните "Backspace"
        </Typography>
      </Box>
    </SidebarWrapper>
  );
}

const SidebarWrapper = styled(Box)(
  ({ theme }) => `
    padding: 40px;
    background: ${theme.sidebar.background};
    width: ${theme.sidebar.width};
    color: ${theme.palette.primary.main};
    min-width: ${theme.sidebar.width};
    box-shadow: ${theme.sidebar.boxShadow};
    position: relative;
    z-index: 5;
    height: 100%;
    padding-bottom: 61px;
`
);
