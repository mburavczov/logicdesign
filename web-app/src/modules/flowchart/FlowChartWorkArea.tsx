import { styled, useTheme } from '@mui/material';
import React, { DragEvent, useCallback, useRef } from 'react';
import ReactFlow, {
  Background,
  Controls,
  MiniMap,
  Node,
  useReactFlow,
  BackgroundVariant
} from 'react-flow-renderer';
import useNodesStateSynced from '@/modules/flowchart/utils/useNodesStateSynced';
import useEdgesStateSynced from '@/modules/flowchart/utils/useEdgesStateSynced';
import { useFlowChart } from '@/modules/flowchart/FlowChartContext';
import {
  ActionNode,
  BeginNode,
  CommentNode,
  ConditionNode,
  EndNode,
  InputNode
} from '@/modules/flowchart/custom-nodes';

const getId = () => `dndnode_${Math.random() * 10000}`;

const nodeTypes = {
  actionNode: ActionNode,
  beginNode: BeginNode,
  commentNode: CommentNode,
  conditionNode: ConditionNode,
  endNode: EndNode,
  inputNode: InputNode,
  default: ActionNode,
  aliasNode: ActionNode
};

const onDragOver = (event: DragEvent) => {
  event.preventDefault();
  event.dataTransfer.dropEffect = 'move';
};

export function FlowChartWorkArea() {
  const theme = useTheme();
  const { nodesMap } = useFlowChart();

  const connectingNodeId = useRef(null);
  const wrapper = useRef<HTMLDivElement>();

  const [nodes, onNodesChange] = useNodesStateSynced();
  const [edges, onEdgesChange, onConnect] = useEdgesStateSynced();

  const { project } = useReactFlow();

  const onDrop = (event: DragEvent) => {
    event.preventDefault();

    if (wrapper.current) {
      const wrapperBounds = wrapper.current.getBoundingClientRect();
      const type = event.dataTransfer.getData('application/reactflow');
      const position = project({
        x: event.clientX - wrapperBounds.x - 80,
        y: event.clientY - wrapperBounds.top - 20
      });
      const newID = getId();
      const newNode: Node = {
        id: newID,
        type,
        position,
        style: {
          border: `1px solid ${theme.palette.primary.main}`,
          backgroundColor: '#fff',
          borderRadius:
            type === 'endNode' || type === 'beginNode' ? '100%' : 10,
          padding: '20px 30px',
          color: theme.palette.primary.main
        },
        data: {
          label: `${type}`,
          id: newID,
          type: type,
          name: '',
          value: ''
        }
      };

      nodesMap.set(newNode.id, newNode);
    }
  };

  const onNodeClick = useCallback((_, node) => {
    const currentNode = nodesMap.get(node.id);
    nodesMap.set(node.id, {
      ...currentNode
    });

    window.setTimeout(() => {
      const currentNode = nodesMap.get(node.id);
      if (currentNode) {
        nodesMap.set(node.id, {
          ...currentNode,
          className: null
        });
      }
    }, 200);
  }, []);

  const onConnectStart = useCallback((_, { nodeId }) => {
    connectingNodeId.current = nodeId;
  }, []);

  return (
    <Wrapper ref={wrapper}>
      <ReactFlow
        nodes={nodes}
        edges={edges}
        snapToGrid={false}
        nodeTypes={nodeTypes}
        onEdgesChange={onEdgesChange}
        onNodesChange={onNodesChange}
        onNodeClick={onNodeClick}
        onConnect={onConnect}
        onConnectStart={onConnectStart}
        onDrop={onDrop}
        onDragOver={onDragOver}
        proOptions={{
          account: 'paid-pro',
          hideAttribution: true
        }}
      >
        <Controls />
        <Background variant={BackgroundVariant.Lines} />
        <MiniMap />
      </ReactFlow>
    </Wrapper>
  );
}

const Wrapper = styled('div')`
  flex-grow: 1;
  height: 100%;
`;
