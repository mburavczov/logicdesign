import { Box, Button, styled } from '@mui/material';
import { Link, Logo } from '@/components';
import { useAuth, useBooleanState } from '@/hooks';
import { useFlowChart } from '@/modules/flowchart/FlowChartContext';
import { RenameDocumentPopup } from '@/modules/flowchart/popups/RenameDocumentPopup';
import { DeleteDocumentPopup } from '@/modules/flowchart/popups/DeleteDocumentPopup';
import { CollaborationPopup } from '@/modules/flowchart/popups/CollaborationPopup';

export function FlowChartHeader() {
  const { isAuthenticated, user } = useAuth();
  const { saveSettings, document } = useFlowChart();
  const [renameDocumentOpen, toggleRenameDocumentOpen] = useBooleanState();
  const [deleteDocumentOpen, toggleDeleteDocumentOpen] = useBooleanState();
  const [collaborationOpen, toggleCollaborationOpen] = useBooleanState();

  return (
    <HeaderWrapper display="flex" alignItems="center">
      <Box display="flex" alignItems="center" gap={1}>
        <Logo />
        {isAuthenticated && (
          <Box display="flex" alignItems="center" gap={0.5}>
            <Button component={Link} href="/dashboard" color="secondary">
              Мои схемы
            </Button>
            /
            <Button color="secondary" onClick={toggleRenameDocumentOpen}>
              {document.name}
            </Button>
          </Box>
        )}
      </Box>
      <Box display="flex" alignItems="center" gap={1}>
        {isAuthenticated ? (
          <>
            <Button variant="contained" onClick={() => saveSettings()}>
              Сохранить
            </Button>
            <Button variant="outlined" onClick={toggleCollaborationOpen}>
              Общий доступ
            </Button>
            {document.ownerId === user.id && (
              <Button
                variant="outlined"
                color="error"
                onClick={toggleDeleteDocumentOpen}
              >
                Удалить
              </Button>
            )}
          </>
        ) : (
          <Button
            component={Link}
            href={`/sign-in?documentId=${document.id}`}
            variant="contained"
          >
            Войти
          </Button>
        )}
      </Box>

      <RenameDocumentPopup
        open={renameDocumentOpen}
        toggleOpen={toggleRenameDocumentOpen}
      />
      <DeleteDocumentPopup
        open={deleteDocumentOpen}
        toggleOpen={toggleDeleteDocumentOpen}
      />
      <CollaborationPopup
        open={collaborationOpen}
        toggleOpen={toggleCollaborationOpen}
      />
    </HeaderWrapper>
  );
}

const HeaderWrapper = styled(Box)(
  ({ theme }) => `
    height: ${theme.header.height};
    color: ${theme.header.textColor};
    padding: ${theme.spacing(0, 2)};
    right: 0;
    top: 0;
    z-index: 6;
    background-color: ${theme.header.background};
    box-shadow: ${theme.header.boxShadow};
    position: fixed;
    justify-content: space-between;
    width: 100%;
`
);
