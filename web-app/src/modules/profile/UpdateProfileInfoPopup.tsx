import { useUpdateUserInfoMutation } from '@/api';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { DialogPopup } from '@/components';
import { Button, CircularProgress, TextField, Typography } from '@mui/material';
import { useAuth } from '@/hooks';

interface UpdateProfileInfoPopupProps {
  open: boolean;
  toggleOpen: () => void;
}

export function UpdateProfileInfoPopup({
  open,
  toggleOpen
}: UpdateProfileInfoPopupProps) {
  const { user, invalidateUser } = useAuth();
  // const updateUserInfo = useUpdateUserInfoMutation({
  //   onSuccess: () => {
  //     toggleOpen();
  //     invalidateUser();
  //   }
  // });
  const formik = useFormik({
    initialValues: {
      id: user?.id,
      name: user?.name,
      surname: user?.surname,
      phone: '',
      avatarPath: ''
    },
    validationSchema: Yup.object({
      name: Yup.string().required('Введите Ваше имя.'),
      surname: Yup.string().required('Введите Вашу фамилию.')
    }),
    onSubmit: async (values): Promise<void> => {
      // await updateUserInfo(values);
    }
  });

  return (
    <>
      <DialogPopup
        header={
          <Typography variant="h4" gutterBottom>
            Редактирование моего профиля
          </Typography>
        }
        actions={
          <>
            <Button color="secondary" onClick={toggleOpen}>
              Отмена
            </Button>
            <Button
              onClick={() => formik.handleSubmit()}
              type="submit"
              startIcon={
                formik.isSubmitting ? <CircularProgress size="1rem" /> : null
              }
              disabled={formik.isSubmitting}
              variant="contained"
            >
              Сохранить
            </Button>
          </>
        }
        open={open}
        onClose={toggleOpen}
      >
        <TextField
          error={Boolean(formik.touched.name && formik.errors.name)}
          fullWidth
          margin="normal"
          helperText={formik.touched.name && formik.errors.name}
          label={'Имя'}
          name="name"
          onBlur={formik.handleBlur}
          onChange={formik.handleChange}
          value={formik.values.name}
          variant="outlined"
        />
        <TextField
          error={Boolean(formik.touched.surname && formik.errors.surname)}
          fullWidth
          margin="normal"
          helperText={formik.touched.surname && formik.errors.surname}
          label={'Фамилию'}
          name="surname"
          onBlur={formik.handleBlur}
          onChange={formik.handleChange}
          value={formik.values.surname}
          variant="outlined"
        />
        <TextField
          error={Boolean(formik.touched.phone && formik.errors.phone)}
          fullWidth
          margin="normal"
          helperText={formik.touched.phone && formik.errors.phone}
          label={'Номер телефона'}
          name="phone"
          onBlur={formik.handleBlur}
          onChange={formik.handleChange}
          value={formik.values.phone}
          variant="outlined"
        />
      </DialogPopup>
    </>
  );
}
