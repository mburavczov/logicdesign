import { StandartPageHeader } from '@/components';
import ProfileCard from '@/modules/profile/ProfileCard';
import { Button, Avatar } from '@mui/material';
import { useBooleanState } from '@/hooks';
import EditIcon from '@mui/icons-material/Edit';
import React from 'react';
import { UpdateProfileInfoPopup } from '@/modules/profile/UpdateProfileInfoPopup';

export function ProfilePage() {
  // const { user } = useAuth();
  const [updateProfileInfoOpen, toggleUpdateProfileInfoOpen] =
    useBooleanState();

  return (
    <>
      <StandartPageHeader
        title="Мой профиль"
        caption="Здесь Вы можете посмотреть и отредактировать информацию о своем профиле"
        action={
          <Button
            sx={{
              m: 1
            }}
            onClick={() => toggleUpdateProfileInfoOpen()}
            variant="contained"
            startIcon={<EditIcon fontSize="small" />}
          >
            Редактировать
          </Button>
        }
        avatar={
          <Avatar
            sx={{
              width: 65,
              height: 65,
              mr: 2
            }}
            src=""
          />
        }
      />
      <ProfileCard />

      <UpdateProfileInfoPopup
        open={updateProfileInfoOpen}
        toggleOpen={toggleUpdateProfileInfoOpen}
      />
    </>
  );
}
