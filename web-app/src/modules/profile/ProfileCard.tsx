import React from 'react';
import { Box, Grid, Typography, Card } from '@mui/material';
import { useAuth } from '@/hooks';

const ProfileCard = () => {
  const { user } = useAuth();

  return (
    <>
      <Box p={4}>
        <Card>
          <Box
            display="flex"
            justifyContent="space-between"
            alignItems="center"
            p={3}
          >
            <Box>
              <Typography variant="h5">Личная информация</Typography>
            </Box>
          </Box>
          <Box p={4}>
            <Grid container spacing={{ xs: 0.5, sm: 2 }}>
              <Grid item xs={4}>
                <Typography variant="subtitle2" textAlign="end">
                  Имя :
                </Typography>
              </Grid>
              <Grid item xs={8}>
                <Typography variant="h5">{user?.name || '-'}</Typography>
              </Grid>
              <Grid item xs={4} textAlign="end">
                <Typography variant="subtitle2">Фамилия :</Typography>
              </Grid>
              <Grid item xs={8}>
                <Typography variant="h5">{user?.surname || '-'}</Typography>
              </Grid>
              <Grid item xs={4}>
                <Typography variant="subtitle2" textAlign="end">
                  Email :
                </Typography>
              </Grid>
              <Grid item xs={8}>
                <Typography variant="h5">{user?.login || '-'}</Typography>
              </Grid>
              <Grid item xs={4}>
                <Typography variant="subtitle2" textAlign="end">
                  Адресс :
                </Typography>
              </Grid>
              <Grid item xs={8}>
                <Typography variant="h5">{user?.state || '-'}</Typography>
              </Grid>
            </Grid>
          </Box>
        </Card>
      </Box>
    </>
  );
};

export default ProfileCard;
