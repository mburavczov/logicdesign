import Head from 'next/head';
import { ProfilePage } from '@/modules/profile';
import { Authenticated } from '@/components';
import { Layout } from '@/layouts';

function Profile() {
  return (
    <>
      <Head>
        <title>Мой профиль | Logic Design</title>
      </Head>
      <ProfilePage />
    </>
  );
}

Profile.getLayout = (page) => (
  <Authenticated>
    <Layout>{page}</Layout>
  </Authenticated>
);

export default Profile;
