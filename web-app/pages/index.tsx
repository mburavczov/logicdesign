import type { ReactElement } from 'react';
import Head from 'next/head';
import { EmptyLayout } from '@/layouts';
import { AboutPage } from '@/modules/about';

function Main() {
  return (
    <>
      <Head>
        <title>О сервисе | Logic Design</title>
      </Head>
      <AboutPage />
    </>
  );
}

Main.getLayout = function getLayout(page: ReactElement) {
  return <EmptyLayout>{page}</EmptyLayout>;
};

export default Main;
