import { Authenticated } from '@/components';
import { Layout } from '@/layouts';
import Head from 'next/head';
import { DashboardPage } from '@/modules/dashboard';

function Dashboard() {
  return (
    <>
      <Head>
        <title>Мои схемы | Logic Design</title>
      </Head>
      <DashboardPage />
    </>
  );
}

Dashboard.getLayout = (page) => (
  <Authenticated>
    <Layout>{page}</Layout>
  </Authenticated>
);

export default Dashboard;
