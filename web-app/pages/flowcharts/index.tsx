import type { ReactElement } from 'react';
import Head from 'next/head';
import { EmptyLayout } from '@/layouts';
import { CreateFlowChartPage } from '@/modules/flowchart/CreateFlowChartPage';

function CreateFlowchart() {
  return (
    <>
      <Head>
        <title>Создание схемы | Logic Design</title>
      </Head>
      <CreateFlowChartPage />
    </>
  );
}

CreateFlowchart.getLayout = function getLayout(page: ReactElement) {
  return <EmptyLayout>{page}</EmptyLayout>;
};

export default CreateFlowchart;
