import type { ReactElement } from 'react';
import Head from 'next/head';
import { EmptyLayout } from '@/layouts';
import { FlowChartPage } from '@/modules/flowchart';

function Flowchart() {
  return (
    <>
      <Head>
        <title>Cхема | Logic Design</title>
      </Head>
      <FlowChartPage />
    </>
  );
}

Flowchart.getLayout = function getLayout(page: ReactElement) {
  return <EmptyLayout>{page}</EmptyLayout>;
};

export default Flowchart;
