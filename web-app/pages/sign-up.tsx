import type { ReactElement } from 'react';
import Head from 'next/head';
import { EmptyLayout } from '@/layouts';
import { RegisterPage } from '@/modules/auth';
import { Guest } from '@/components';

function SignUp() {
  return (
    <>
      <Head>
        <title>Регистрация | Logic Design</title>
      </Head>
      <RegisterPage />
    </>
  );
}

SignUp.getLayout = function getLayout(page: ReactElement) {
  return (
    <Guest>
      <EmptyLayout>{page}</EmptyLayout>
    </Guest>
  );
};

export default SignUp;
