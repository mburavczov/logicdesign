import type { ReactElement } from 'react';
import Head from 'next/head';
import { EmptyLayout } from '@/layouts';
import { LoginPage } from '@/modules/auth';
import { Guest } from '@/components';

function SignIn() {
  return (
    <>
      <Head>
        <title>Вход | Logic Design</title>
      </Head>
      <LoginPage />
    </>
  );
}

SignIn.getLayout = function getLayout(page: ReactElement) {
  return (
    <Guest>
      <EmptyLayout>{page}</EmptyLayout>
    </Guest>
  );
};

export default SignIn;
